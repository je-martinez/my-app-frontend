# My App Frontend

A basic example of a static HTML

# How to use CD / CI

In order to test on a **S3 Bucket**, you need to set the next environment variables:

1. `AWS_ACCESS_KEY_ID`
2. `AWS_SECRET_ACCESS_KEY`
3. `AWS_DEFAULT_REGION`
4. `S3_BUCKET`
5. `DISTRIBUTION_ID`

To create an Amazon Access Key and Secret Access Key you can check the next documentation:

https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html

On your **AWS Console**, go to the **S3** Section, here you can get the `S3_BUCKET`.

On your **AWS Console**, go to the **Cloudfront** Section, here you can get the `DISTRIBUTION_ID` this is necessary to update with the new html everytime we build or publish a new change.
